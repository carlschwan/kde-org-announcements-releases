---
title: KDE's MONTH 2020 Apps Update

publishDate: 2020-04-23 16:00:00

layout: page # don't translate

summary: "What Happened in KDE's Applications This Month"

type: announcement # don't translate
---

git diff 'HEAD@{1 month ago}' HEAD

ssh ftpadmin@master.kde.org 'find stable/ -mtime 31'

https://mail.kde.org/pipermail/kde-announce-apps/

https://mail.kde.org/pipermail/kde-announce/

Intro Paragraph

## New releases

### 18.04 Releases

New features in 18.04 releases

[19.12.2 release notes](https://community.kde.org/Releases/19.12_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;  [19.12.1 source info page](https://kde.org/info/releases-19.12.1) &bull; [19.12.1 full changelog](https://kde.org/announcements/changelog-releases.php?version=19.12.1)

### krita new release

##" Plasma bigscreen announce?

### Smb4K new release

### KDialog new release

### kio-gdrive 1.3

## Incoming

Basket and big screen repos made

## App Store

Google Play?

## Website Updates

Any new websites this month?

# KDE AppImage

## What is an AppImage?

AppImage is a packaging format that provides a way for upstream developers to provide “native” binaries for GNU/Linux users just the same way they could do for other operating systems. It allow packaging applications for any common Linux based operating system, e.g., Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora etc. AppImages come with all dependencies that cannot be assumed to be part of each target system in a recent enough version and will run on most Linux distributions without further modifications.

## What you need to install to have them running best?

To run an AppImage you need the Linux kernel (> 2.6) and `libfuse`. Those dependency are present in the majority of the GNU/Linux distributions so you have no need to install anything special.

Sadly the AppImage support on the major desktop environments (KDE and GNome) is not complete so you may require an additional tool to create a menu entry of your app. In such cases depending on the UX you preffer you can choose between:
- [AppImageLauncher](https://www.appimagehub.com/p/1228228) for a first-run integration pop-up or
- [appimaged](https://github.com/AppImage/appimaged) for a auntomated intagration of every AppImage deployed in your home dir.

To update your AppImages you use [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). This is embed in AppImageLauncher so if you already installed it, you have no need to install anithing aditional. Just right click over the AppImage file and choose update. Notice that not all packagers embed the update information into the binaries so there may be cases in which you will have to manually download the new version. 


## What kde apps run with them?

There are several KDE apps that are already being distributed as AppImage, the more relevant ones are:
- kdenlive
- krita
- kdevelop

## What is appimagehub?

The recommended way of getting AppImages is from the original application authors, but this is not quite practical if you still don't know which app do you need. There is when [appimagehub.com]() comes in. It's a software store dedicated only to AppImages. There you can find a catalog with more than 600 apps for your daily tasks. 

This web site is part of the [opendesktop.org]() platform which provides a complete ecosystem for users and developers of free and open source applications.

## How to make an AppImage?

Making an AppImage is all about bundling all your app dependencies into a single dir (AppDir). Then it's bundled into a sqaushfs image and appended to a 'runtime' that allows it's execution.

To acomplish this task you can use the following tools:

- https://github.com/linuxdeploy
- https://github.com/AppImageCrafters/appimage-builder

In the [AppImage Package Guide](https://docs.appimage.org/packaging-guide/index.html) you can find an extensive documentation of how to use such tools. Also you can join the [AppImage IRC channel](irc://irc.freenode.net/#AppImage)

## Stores

KDE software is available on many platforms and software app stores.

[![Snapcraft](../platforms/snapcraft.png)](https://snapcraft.io/publisher/kde)
[![Flathub](../platforms/flathub.png)](https://flathub.org/apps/search/kde)
[![Microsoft Store](../platforms/microsoft.png)](https://www.microsoft.com/en-gb/search?q=kde)
Google Play link?
F-Droid link?
