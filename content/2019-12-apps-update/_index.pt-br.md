---
title: Atualização de dezembro de 2019 dos aplicativos da KDE

summary: "O que aconteceu com o KDE Applications este mês"

publishDate: 2019-12-12 13:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

# Novas versões dos aplicativos da KDE sendo lançadas em dezembro

O lançamento das novas versões dos aplicativos da KDE são parte do esforço
contínuo da comunidade em fornecer um catálogo completo e atualizado de
programas úteis, belíssimos e cheios de funcionalidades para o seu sistema.

Agora estão disponíveis novas versões do navegador de arquivos Dolphin; do
Kdenlive, um dos editores de vídeo de código aberto mais completos que há; o
visualizador de documentos Okular; o visualizador de imagens Gwenview; e
todos os seus programas e utilitários da KDE preferidos. Todos estes
aplicativos foram melhorados e se tornaram mais rápidos e estáveis,
esbanjando de funcionalidades novas e bem animadoras. As novas versões dos
aplicativos do KDE permitem que você seja produtivo e criativo ao mesmo
tempo em que tornam o uso de programas da comunidade KDE fácil e divertido.

Esperamos que você aproveite bastante as novas funcionalidades e melhorias
agora presentes em todos os seus aplicativos da KDE!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) Repaginado

[O Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan)
teve um importante lançamento em dezembro. A ferramenta de planejamento e
gerenciamento de projetos da comunidade KDE recebeu uma atualização
histórica desde a última versão lançada quase dois anos atrás.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

O Calligra Plan permite a você gerenciar pequenos e grandes projetos usando
vários recursos. O Plan oferece diferentes tipos de dependências de tarefas
e datas limite para você modelar o seu projeto. É possível definir tarefas,
estimar o esforço necessário para cada uma delas, alocar recursos e agendar
o projeto de acordo com suas necessidades e recursos disponíveis.

Um dos pontos fortes do Plan é o seu excelente suporte a diagramas de
Gantt. Esse tipo de diagrama é composto de barras horizontais que fornecem
uma ilustração gráfica de um compromisso e ajuda a planejar, coordenar e
monitorar tarefas específicas de um projeto. O uso de diagramas de Gantt no
Plan ajudam a gerenciar melhor o fluxo do seu projeto.

## Impulsionando o volume no [Kdenlive](https://kdenlive.org)

Os desenvolvedores do [Kdenlive](https://kdenlive.org) incluíram novas
funcionalidades e sumiram com erros numa velocidade impressionante. Só esta
versão já vem com mais de 200 commits.

Bastante esforço foi feito para melhorar o suporte ao áudio. Na categoria
"erros solucionados", o Kdenlive se livrou de um problema de alto consumo de
memória, ineficiência nas miniaturas de áudio e ficou com o armazenamento
mais rápido.

Mais animador ainda é o fato de o Kdenlive agora vir com um impressionante
novo mixer de sons (ver imagem). Os desenvolvedores também incluíram uma
nova exibição de clipes de áudio no Monitor de clipes e o Arquivos do
projeto para que você possa sincronizar melhor seu movimento de imagens com
a trilha sonora.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="O mixer de áudio do Kdenlive" >}}

Já quanto aos detalhes finos, o Kdenlive recebeu várias melhorias de
desempenho e usabilidade e os desenvolvedores corrigiram múltiplos erros na
sua versão para o Windows.

## Pré-visualização e navegação do [Dolphin](https://userbase.kde.org/Dolphin)

O robusto gerenciador de arquivos
[Dolphin](https://userbase.kde.org/Dolphin) inclui funcionalidades novas
para te ajudar a encontrar e chegar ao que você estiver procurando no seu
sistema de arquivos. Uma dessas funcionalidades são as novas opções de
pesquisa avançada, outra é a possibilidade de voltar e avançar no histórico
de locais que você já visitou antes pressionando e segurando o ícone de
flecha na barra de ferramentas. Já a função que permite que você vá
rapidamente para locais ou arquivos acessados recentemente foi retrabalhada
e refindada.

Uma das maiores preocupações que os desenvolvedores do Dolphin tem é de
permitir que seus usuários possam pré-visualizar o conteúdo dos seus
arquivos antes que sequer sejam abertos. Por exemplo, na nova versão do
Dolphin é possível pré-visualizar GIFs no painel de pré-visualização ao
mantê-los selecionados. Também é possível clicar e reproduzir vídeos e áudio
no mesmo painel.

Se você é um fã de graphic novels, o Dolphin agora é capaz de exibir miniaturas para revistas em quadrinhos no formato .cb7 (também dê uma olhada no suporte do Okular para este formato -- veja abaixo). Já mencionando outro fato a respeito das miniaturas, ampliá-las é algo que o Dolphin já faz há bastante tempo: segure <kbd>Ctrl</kbd>, mova a roda do mouse e a miniatura irá aumentar ou diminuir. Uma nova funcionalidade quanto a isso é que agora é possível redefinir o zoom para o padrão apertando <kbd>Ctrl</kbd> + <kbd>0</kbd>.

Ainda outra função útil do Dolphin é sua capacidade de mostrar quais
programas estão impedindo um dispositivo de ser desmontado.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): Faça seu celular controlar seu computador

A última versão do [KDE Connect](https://community.kde.org/KDEConnect) está
cheia de novas funcionalidades. Uma das mais visíveis é o novo app de SMS
que permite que você leia e escreva SMS com o histórico de conversas
inteiro.

Uma nova interface de usuário baseada no
[Kirigami](https://kde.org/products/kirigami/) permite que você possa rodar
o KDE Connect não somente no Android, mas também em todos os dispositivos
móveis com Linux que veremos em aparelhos futuros como o PinePhone e o
Librem 5. A interface Kirigami também fornece novas funcionalidades para
usuários de desktop como controles de mídia, teclado e mouse remotos, ligar
para o dispositivo, transferência de arquivos e execução de comandos.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="A nova interface da sua área de trabalho" >}}

Você já é capaz de usar o KDE Connect para controlar o volume de mídia sendo
reproduzida no seu computador, mas agora você pode usá-lo para controlar o
volume do sistema também -- super conveniente quando estiver usando seu
celular como controle remoto. Quando estiver fazendo uma apresentação, você
também pode controlar sua apresentação usando o KDE Connect para avançar e
voltar nos seus slides.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Outra função já presente há algum tempo é a possibilidade de transferir
arquivos para o seu celular, mas esta nova versão permitirá que você abra o
arquivo imediatamente após o arquivo ser recebido. O KDE Itinerary usa isso
para enviar informações de viagem do KMail para seu celular.

Além disso, agora também é possível enviar arquivos do Thunar (o gerenciador
de arquivos do XFCE) e de aplicativos do Elementary como o Arquivos do
Pantheon. A exibição do progresso de cópia de múltiplos arquivos foi
bastante melhorada e arquivos recebidos do Android agora exibem o horário
correto de modificação.

Em se tratando das notificações, agora é possível ativar ações das
notificações do Android no computador e o KDE Connect pode utilizar-sede
funcionalidades avançadas do centro de notificações do Plasma para fornecer
notificações melhores. A notificação de confirmação de pareamento agora
permanece presente para que você não perca seu uso mais.

Por fim, se estiver usando o KDE Connect na linha de comando, o
*kdeconnect-cli* teve seu preenchimento automático do *zsh* melhorado.

## Imagens remotas no [Gwenview](https://userbase.kde.org/Gwenview)

O [Gwenview](https://userbase.kde.org/Gwenview) agore permite o ajuste do
nível de compressão JPEG quando você for salvar imagens editadas. Os
desenvolvedores também melhoraram o desempenho para imagens remotas e o
Gwenview agora pode importar fotos tanto de e para locais remotos.

## [Okular](https://kde.org/applications/office/org.kde.okular) agora com suporte ao formato .cb7

O visualizador de documentos
[Okular](https://kde.org/applications/office/org.kde.okular) que te permite
ler, anotar e verificar todo tipo de documentos, incluindo PDFs, EPubs, ODTs
e MDs, agora tem suporte a arquivos de revistas em quadrinhos do formato
.cb7.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): Um reprodutor de música simples e completo

O reprodutor de música
[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) combina
simplicidade e elegância numa interface moderna. Em sua versão mais atual, o
Elisa teve sua aparência otimizada para se adaptar melhor a telas de High
DPI. Ele também se integra melhor com outros aplicativos do KDE e recebeu
suporte ao sistema de menu global da KDE.

A indexação de arquivos de música também foi melhorada e o Elisa agora
suporta rádios online e vem com alguns exemplos para você testar.

{{< img src="elisa.png" caption="A interface atualizada do Elisa" >}}

## O [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) agora pronto para telas de toque

O [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), o
programa padrão da comunidade KDE para captura de tela, é outro aplicativo
que adquiriu abilidades de tela de toque: seu novo redimensionador amigável
ao toque torna muito mais fácil formar retângulos para capturar as partes da
sua área de trabalho que você gostaria em dispositivos táteis.

Outras melhorias incluem a nova função de salvamento automático, bem
conveniente para fazer múltiplas capturas de tela rapidamente, e as barras
de progresso animado, que garantem que você possa ver o que o Spectacle está
fazendo no momento.

{{<img style="max-width:500px" src="spectacle.png" caption="O novo redimensionador do Spectacle" >}}

## Melhorias na [Integração de navegadores do Plasma](https://community.kde.org/Plasma/Browser_Integration)

O novo lançamento da [Integração de navegadores do
Plasma](https://community.kde.org/Plasma/Browser_Integration) agora possui
uma lista de bloqueio para a função de controles de mídia. Isto é útil para
quando você visitar um site com bastante mídia disponível para reproduzir, o
que pode tornar difícil selecionar o que você gostaria de controlar do seu
computador. A lista de bloqueio permite que você exclua esses sites dos
controles de mídia.

A nova versão também permite que você mantenha o URL original nos metadados
do arquivo e inclui suporte à API Web Share. Essa API permite que
navegadores compartilhem links, texto e arquivos com outros aplicativos do
mesmo modo que programas da KDE, melhorando a integração de navegadores não
nativos como o Firefox, Chrome/Chromium e Vivaldi com o resto dos
aplicativos da KDE.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Loja da Microsoft

Uma boa quantidade de aplicativos da comunidade KDE podem ser baixados na
Loja da Microsoft. O Krita e o Okular já estão disponíveis lá há algum
tempo, mas agora o o editor de LaTeX fácil de usar, o
[Kile](https://kde.org/applications/office/org.kde.kile), uniu-se ao grupo.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Novo

O que nos leva aos recém-chegados: o
[SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), um
aplicativo que permite criar facilmente legendas para vídeos, agora está no
Incubador da KDE, ou seja, a caminho de se tornar um membro integral da
família de aplicativos da comunidade KDE. Seja bem-vindo!

Enquanto isso, o [plasma-nano](https://invent.kde.org/kde/plasma-nano), uma
versão mínima do Plasma feita para dispositivos embarcados, está pronta para
o lançamento do Plasma 5.18 e estará nos repositórios.

## Lançamentos 19.12

Alguns dos nossos projetos lançam a seu próprio tempo e alguns são lançados
em lotes. O conjunto de projetos do 19.12 foi lançado hoje e deve ficar
disponível nas lojas de aplicativos e distribuições em breve. Dê uma olhada
na [página de lançamentos do
19.12](https://www.kde.org/announcements/releases/19.12). Este conjunto era
chamado anteriormente de KDE Applications mas teve sua nomeação alterada
para se tornar um serviço de lançamentos, deste modo evitando confusão com
todos os outros aplicativos da KDE e também por ser dezenas de produtos
diferentes ao invés de um conjunto só.
